const fs = require("fs");
const archiver = require("archiver");
const path = require("path");
const { createJsonFile } = require("./createJSON");

let index = 0;

async function archiveFolder(rootFolder) {
  // create a promise array to handle async process
  const promises = [];

  // get all subfolders within the root folder
  fs.readdirSync(rootFolder, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .forEach((dirent) => {
      const folder = dirent.name;
      const folderPath = path.join(rootFolder, folder);
      fs.readdirSync(folderPath).forEach((childFolder) => {
        if (childFolder !== ".DS_Store") {
          let logo = "";

          const childPath = path.join(folderPath, childFolder);
          console.log(childPath);
          // if (index === 1) return;
          index++;

          // check is logo exist
          if (fs.existsSync(`${childPath}/logo.webp`)) {
            fs.renameSync(`${childPath}/logo.webp`, `./logo/${childFolder}.webp`);
            logo = `https://gitlab.com/tranhuy.beearning/nintendo-64-games-2/-/raw/main/logo/${childFolder}.webp`;
            console.log(`File moved successfully. ${childPath}`);
          } else if (fs.existsSync(`./logo/${childFolder}.webp`)) {
            logo = `https://gitlab.com/tranhuy.beearning/nintendo-64-games-2/-/raw/main/logo/${childFolder}.webp`;
            console.log(`Has Logo-----> ${childPath}`);
          }
          createJsonFile(childFolder, logo);
          if (fs.existsSync(`./zip/${childFolder}.zip`)) {
            console.log(`./zip/${childFolder}.zip already exist !!!!!`);
          } else {
            promises.push(
              new Promise(async (resolve, reject) => {
                // create a new archive
                const archive = archiver("zip", {
                  zlib: { level: 9 },
                });

                // specify the output file
                const output = fs.createWriteStream(path.join("./zip", `${childFolder}.zip`));

                // listen for errors
                archive.on("error", (err) => {
                  reject(err);
                });

                // pipe the archive to the output file
                archive.pipe(output);

                // get all files within the childFolder
                archive.directory(childPath, childFolder);

                // finalize the archive
                archive.finalize();

                // listen for the 'close' event, which indicates that the archive is complete
                output.on("close", () => {
                  console.log(`Archive for ${childFolder} created: ${archive.pointer()} total bytes`);
                  resolve();
                });
              })
            );

            // // creates a new file with the name archiveName
            // const output = fs.createWriteStream(path.join("./zip", `${childFolder}.zip`));

            // // pipe archive data to the file
            // archive.pipe(output);

            // console.log({ childPath });
            // index++;
            // //   add all files inside the subfolder to the archive
            // archive.directory(childPath, childFolder);
          }
        }
      });
    });

  //   loop through each subfolder
  //   for (const subfolder of subfolders) {

  //   }
  console.log({ promises });

  await Promise.all(promises);
  console.log("All archive done!");
}

archiveFolder("./nintendo-64");

// zipFolders("./gameboy-advance");
